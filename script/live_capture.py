#!/usr/bin/env python

import os
import sys
import mysql.connector
import subprocess

CWD = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.dirname(CWD)
sys.path.append(ROOT_DIR)

from zk import ZK
from mysql.connector import Error

mydb 	= None
conn 	= None
hsl 	= None
cursor	= None
zk 		= ZK('10.121.0.99', port=4370)

try:
    conn = zk.connect()
    for attendance in conn.live_capture():
        if attendance is None:
            pass
        else:
		
			try:
				mydb = mysql.connector.connect(host="localhost", database="att", user="root", password="")
				hsl				=str(attendance)
				rst 			=hsl.split("#")
				
				if(mydb.is_connected()):
				
					mydb.autocommit	=False
					cursor			=mydb.cursor(prepared=True)
					strSQL 			="INSERT INTO att_raw(id_aphris, tgl, stat) VALUES(%s, %s, %s)"
					val				=(rst[0],rst[1],rst[3])
					result			=cursor.execute(strSQL,val)
					mydb.commit()
					
					conn.disable_device()
					conn.clear_attendance()
					conn.enable_device()
				
			except Error as er:
				print("Error while connecting to MySQL : {}".format(er))
				mydb.rollback()
			finally:
				if(mydb.is_connected()):
					cursor.close()
					mydb.close()			
	
except Exception as e:
	print("Process terminate : {}".format(e))
finally:
    if conn:
        conn.disconnect()
	